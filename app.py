import time
import redis
from flask import Flask
import os

cache_host = os.environ["CACHE_HOST"];
cache_port = os.environ["CACHE_PORT"];

app = Flask(__name__)
cache = redis.Redis(host=cache_host, port=cache_port);

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Funcionando. Eu fui visto {} vezes.\n'.format(count)

if __name__=="__main__":
    app.run(host="0.0.0.0", debug=True);
